# Workoutime
- Um projeto que possibilita ao atleta inserir suas atividades e verificar
sua evolução através de uma visualização gráfica.

# Projeto

## ReactJS
- Biblioteca que possibilita a criação de uma Single Page Application
com a criação de componentes.
- Aumenta a reusabilidade e criação de componentes com responsabilidade única.

## Typescript
- Possibilita a criação de tipos de acordo com a necessidade do negócio.

# Estrutura

## Pasta Components
- Ficarão todos os componentes que serão apresentados nas telas.
- Uma tela pode ter mais de um componente. E um componente pode utilizar outros.
- Há 3 componentes criados por mim:

### Header
- Header que será utilizado por todos os componentes dentro da página principal.
- Apresenta a logo da aplicação, o menu para escolha das visualizações e uma
opção para o usuário fazer o logout da aplicação.

### Dashboard
- Componente que irá apresentar as atividades físicas salvas pelo usuário.
- Possibilita que ele insira novas atividades ou remova as que desejar.
- As atividades estão sempre sendo ordenas pela data.
- Nessa versão não há ainda a opção de editar um atividade. Somente inserção
e remoção.

### Grafico
- Apresenta o desempenho do atleta de acordo com as atividades inseridas no
sistema em um gráfico de preencimento da área.
- Para isso as atividades são agrupadas por data e para cada data o tempo
de todas as atividades são somadas.
- Visualização: quantidade de minutos de atividade/dia.
- Há espaço para melhorar e apresentar mais visualizações como a quantidade
de tempo por tipo de ativadade, etc.

## Pasta Pages
- Ficarão as nosssa páginas.
- Dentro das páginas os componentes serão utilizados de acordo com a necessidade.
- Há basicamente 3 páginas na aplicação.

### Página de login
- Usuário insere seus dados (email e senha) para entrar na aplicação.

### Página de cadastro
- Usuário deve inserir os dados para cadastrar na aplicação (nome, email e
senha).

### Página principal
- Página que fará a apresentação dos componentes de acordo com a visão
escolhida pelo usuário no menu header.

## Pasta hooks - Conteito de Context API
- Utilizei o novo conceito do React que "substitui" o uso do Redux.
- Context API. Dessa forma podemos criar variáveis globais que poderão ser
utilizadas por aqueles componentes que forem permitidos pelo desenvolvedor.
- Como criamos um hook para cada contexto, para facilitar a chamada. Chamei
a pasta de hooks.
- Na aplicação há dois contextos de aplicação.

### Contexto de autenticação
- Os dados do usuário logado ficam disponíveis para serem utilizados em todos
os componentes dentro da página principal.

### Contexto de atividades físicas do usuário
- Todas as atividades do usuário são buscadas na API, assim que ele se loga
no sistema. Todas essas atividades ficam salvas no contexto da aplicação,
para que se o usuário alternar para um componente onde seja necessário somente
a visualização das atividades, não há necessidade de buscar tudo na API
novamente.

## Pasta __tests__
- Vai armazenar todos os testes da aplicação.
- Dentro dela, dividi em mais pastas:
  - components;
  - pages;
  - util;
  - context;
- Dessa forma os testes ficam melhor organizados.

## Pasta SCSS
- Utilizei o pré-processador de script SASS com a extensão SCSS. Dessa forma,
pode-se utilizar os benefícios do SASS com uma linguagem mais parecida com o
css puro, utilizando chaves.
- Outra abordagem que gosto bastante é de utilizar o styled components e
criar cada arquivo de estilização dentro da pasta de cada componente ou
página.
- Contudo, nesse projeto, há uma pasta scss em que estão todas as estilizações
com o mesmo nome de cada componente e um base.scss (principal) que importa
todos os outros .scss e é aplicado no componente principal. Dessa forma toda
a árvore pode ser estilizada.

### Plugins SCSS
- Instalei no vscode dois plugins para compilar o scss para css.
  - Live Sass Compiler
  - SCSS Formatter

## Pasta Util
- Algumas funções que devem estar isoladas, pois não fazem parte da
responsabilidade de nenhum componente específico.

## Pasta Services
- Fica a configuração para conexão com a API do backend.

## Pasta DTO - Data transfer object
- São as classes e interfaces que irão representar os dados que devem trafegar
entre os componentes da aplicação.
- Dessa forma os dados trafegam fortemente tipados e facilita a validação e
percepção de erros.

# Iniciando o projeto

## Create react app
- https://www.npmjs.com/package/create-react-app
- Esse pacote possibilita criar um projeto react com o babel e webpack já
 configurados. Entretanto, não impede de futuras customizações que forem
 necessárias para o projeto.
 - Babel: Converte o código do React (JSX: html dentro do javascript) para
 um código que o browser entenda (javascript puro).
 - Webpack: Configura-se os tipos de conversões necessárias para cada tipo
 de arquivo: .js, .css, .png, etc.
    - Dentro do webpack são configurados os loaders.

## Qualidade do código e Engenharia de softeware

### Commitlint
- https://github.com/conventional-changelog/commitlint
- Acusa um erro quando o dev tentar fazer um commit fora do padrão: https://www.conventionalcommits.org/en/v1.0.0/

### Husky
- Vai permitir a configuração dos HOOKS do git.
- Dessa forma podemos definir as ações que devem ser feitas antes de um commit, por exemplo.

### Commitzen
- Vai criar uma interface visual na linha de comando para facilitar que o desenvolvedor consiga seguir o convetional commit, através de um template.
- Vai criar um padrão que segue o config conventional da comunidade do angular.
- Nada impede que o time crie seus próprios padrões e troque o caminho da pasta dentro do package.json.
- Adiciona-se um script dentro do package.json para que, quando executado com o yarn, o template seja iniciado. ("commit": "git-cz")
- Executar: yarn commit
- Adição de mais uma configuração no husky para que o dev possar iniciar o template ao digitar git commit.
```
"prepare-commit-msg": "exec < /dev/tty && git cz --hook || true"
```
- https://github.com/commitizen/cz-cli


## Editorconfig
- Auxilia na padronização da configuração para vários desenvolvedores
trabalhando em um mesmo projeto, mas em diferentes editores de código ou IDE's.
```
root = true

[*]
indent_style = space
indent_size = 2
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
end_of_line = lf
```

## Eslint
- Adicionar o plugin ao vscode
  - https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

- Configuração necessária no VScode
  - Crtl + Shift + p
  ```
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
  ```

- yarn add eslint@6.8.0 -D
- yarn eslint --init
```
❯ yarn eslint --init
yarn run v1.22.5
$ /media/snowman/b3db83e2-fb87-46a1-bcf8-0d77015a3479/luiz/workoutime/frontend/node_modules/.bin/eslint --init
? How would you like to use ESLint? To check syntax, find problems, and enforce code style
? What type of modules does your project use? JavaScript modules (import/export)
? Which framework does your project use? React
? Does your project use TypeScript? Yes
? Where does your code run? Browser
? How would you like to define a style for your project? Use a popular style guide
? Which style guide do you want to follow? Airbnb: https://github.com/airbnb/javascript
? What format do you want your config file to be in? JSON
```
- Não instale os pacotes com npm se tiver usando yarn. Caso contrário aperte sim.
- Se tiver usando yarn:
```
yarn add -D eslint-plugin-react@^7.20.0 @typescript-eslint/eslint-plugin@latest eslint-config-airbnb@latest eslint-plugin-import@^2.21.2 eslint-plugin-jsx-a11y@^6.3.0 eslint-plugin-react-hooks@^4 @typescript-eslint/parser@latest
```
- Para que o ReactJS consiga entender arquivos Typescript nas importações é necessário acrescentar uma configuração adicional pois por padrão vai ser apresentado um erro dizendo que as importações de arquivos Typescript não foram resolvidas, para resolver isso basta instalar uma dependência que habilite essa funcionalidade:
```
yarn add eslint-import-resolver-typescript -D
```
- Nosso arquivo .eslint.json deve ficar da seguinte maneira:
```
{
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "plugin:react/recommended",
        "airbnb",
        "plugin:@typescript-eslint/recommended",
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "@typescript-eslint",
        "react-hooks"
    ],
    "rules": {
      "react-hooks/rules-of-hooks": "error",
      "react-hooks/exhaustive-deps": "warn",
      "react/jsx-filename-extension": [1, { "extensions": [".tsx"] }],
      "import/prefer-default-export": "off",
      "camelcase": "off",
      "@typescript-eslint/ban-types": "off",
      "settings": {
        "import/resolver": {
          "typescript": {}
        }
      },
      "import/extensions": [
        "error",
        "ignorePackages",
        {
          "ts": "never",
          "tsx": "never"
        }
      ]
    }
}

```

# Prettier
- Podemos seguir as instruções desse arquivo:
https://www.notion.so/Prettier-e2c6a3ec188c4cce8890a3e16a0d6425#da104f9b05964ec1aaac067ab2bf8a54

# Componentização
- Divisão da aplicação em componentes com responsabilidade bem definidas.

# DTO
- Padrão para transfência de dados entre componentes da aplicação. É feita
a tipagem utilizando uma interface ou uma classe para cada tipo de "entidade".

# Testes

## React testing library
- Já vem adicionada quando iniciamos a aplicação com create-react-app.
- https://testing-library.com/docs/react-testing-library/intro
- Criei um exemplo de teste utilizando essa biblioteca junto do jest.

### Caso de teste - página de login (SignIn)
- Teste 1: verifica se, após o usuário inserir um email e senha válidos e
clicar no botão de entrar, ele é redirecionado para a página principal.
- Teste 2: verifica se, no caso de o usuário inseir um email inválido, ele
não é redirecionado.

## Testing library react-hooks
- https://github.com/testing-library/react-hooks-testing-library
- Utilizei essa biblioteca para testar os hooks.
- Há necessidade de adicionar também a lib: yarn add react-test-renderer -D
- Criei um exemplo de teste para o hook de autenticação do usuário.

### Caso de teste - Contexto de autenticação
- Teste 1: verifica se o usuário consegue realizar um login na aplicação e
se os dados são salvos no localStorage.
- Teste 2: verifica se o contexto de autenticação recupera corretamente
os dados do localStorage de um usuário já logado na aplicação.
- Teste 3: verifica se a função de logout está funcionando corretamente.

## Axios mock adapter
- Lib utilizada para fazer mock de api.
- https://github.com/ctimmerm/axios-mock-adapter

## Coverage Report
- Adicionar dentro do package.json
```
"jest": {
  "collectCoverageFrom": [
    "src/pages/**/*.tsx",
    "src/components/**/*.tsx",
    "src/hooks/**/*.tsx",
    "src/util/**/*.ts"
  ]
},
```
- Para executar o coverage, execute:
```
yarn test --coverage --watchAll false
```
- Com o coverage, podemos verificar o quanto do nosso código está coberto por
testes.
- O código não está coberto 100%, pois não criei todos os testes. Criei,
somente aqueles que achei interessante para demonstrar o funcionamento das libs
mostradas acima.

/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from 'react';

import {
  RouteProps as ReactDOMRouteProps,
  Route as ReactDOMRoute,
  Redirect,
} from 'react-router-dom';
import { useAuth } from './hooks/AuthContext';

interface RouteProps extends ReactDOMRouteProps {
  isPrivate?: boolean;
  component: React.ComponentType;
}

/**
 * Componente criado para melhorar o Route do react-router-dom.
 * Dessa forma, conseguimos adicionar novas propriedades para os componentes.
 * Foi criado a propriedade isPrivate. Os componentes que tiverem essa
 * propriedade só poderão se acessados se o usuário estiver logado na
 * aplicação.
 * @param param0
 */
const Route: React.FC<RouteProps> = ({
  isPrivate = false,
  component: Component,
  ...rest
}) => {
  const { usuario } = useAuth();
  return (
    <div>
      <ReactDOMRoute
        {...rest}
        render={() => {
          /**
           * A verificação feita é a seguinte: a rota é privada e o usuário
           * está logado, então apresenta o componente. Caso contrário,
           * o usuário é sempre redirecionado para a página de login.
           */
          return isPrivate === !!usuario ? (
            <Component />
          ) : (
            <Redirect to={{ pathname: isPrivate ? '/' : 'main' }} />
          );
        }}
      />
    </div>
  );
};

export default Route;

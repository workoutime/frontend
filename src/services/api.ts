import axios from 'axios';

/**
 * API pronta para funcionar em produção e em ambinente dev.
 */
const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL || 'http://localhost:3331',
});

export default api;

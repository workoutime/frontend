import React from 'react';
import Routes from './routes';
import './scss/base.css';

/**
 * Componente principal.
 */
const App: React.FC = () => {
  return (
    <div className="App">
      <Routes />
    </div>
  );
};

export default App;

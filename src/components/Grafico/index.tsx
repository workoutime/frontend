/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable func-names */
/* eslint-disable no-param-reassign */
/* eslint-disable import/no-duplicates */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useEffect, useState } from 'react';
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label,
  ResponsiveContainer,
} from 'recharts';
import { useAtividadesFisicas } from '../../hooks/AtividadesContext';
import montarArrayDeAtividadesPorData from '../../util/MontaArrayDeAtividadesPorData';

interface IAtividadesPorData {
  data: string;
  tempo: number;
}

const Grafico: React.FC = () => {
  const { atividadesContext } = useAtividadesFisicas();
  const [atividadesPorData, setAtividadesPorData] = useState<
    IAtividadesPorData[]
  >([]);

  useEffect(() => {
    setAtividadesPorData(montarArrayDeAtividadesPorData(atividadesContext));
  }, []);

  return (
    <div id="grafico">
      <div id="graficoTexto">
        <h3>Esse é seu desempenho de atividades em minutos por dia</h3>
      </div>
      <div style={{ width: '100%', height: 500 }}>
        <ResponsiveContainer>
          <AreaChart
            data={atividadesPorData}
            margin={{
              top: 10,
              right: 30,
              left: 0,
              bottom: 0,
            }}
          >
            <CartesianGrid strokeDasharray="5 5" />
            <XAxis dataKey="data">
              <Label value="Dia" offset={2} position="insideBottom" />
            </XAxis>
            <YAxis
              label={{ value: 'minutos', angle: -90, position: 'insideLeft' }}
            />
            <Tooltip />
            <Area
              type="monotone"
              dataKey="tempo"
              stroke="rgb(5, 192, 89)"
              fill="rgb(5, 192, 89)"
            />
          </AreaChart>
        </ResponsiveContainer>
      </div>
    </div>
  );
};

export default Grafico;

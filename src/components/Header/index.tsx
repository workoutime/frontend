/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Dispatch, SetStateAction } from 'react';
import logo from '../../assets/running.svg';
import { useAuth } from '../../hooks/AuthContext';

interface IHeader {
  setComponent: Dispatch<SetStateAction<string>>;
}
const Header: React.FC<IHeader> = ({ setComponent }) => {
  const { signOut } = useAuth();

  return (
    <div id="header">
      <div className="logo">
        <img src={logo} alt="time" />
        <h1>Workoutime</h1>
      </div>
      <div className="menu">
        <p onClick={() => setComponent('painel')}>painel</p>
        <p onClick={() => setComponent('grafico')}>gráfico</p>
        <p onClick={signOut}>logout</p>
      </div>
    </div>
  );
};

export default Header;

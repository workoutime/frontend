/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/no-duplicates */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import DatePicker, { registerLocale } from 'react-datepicker';
import pt from 'date-fns/locale/pt';
import 'react-datepicker/dist/react-datepicker.css';
import { ToastContainer, toast } from 'react-toastify';
import AtividadeDTO from '../../DTO/AtividadeDTO';
import api from '../../services/api';
import IShowAtividadeDTO from '../../DTO/IShowAtividadeDTO';
import { useAuth } from '../../hooks/AuthContext';

import 'react-toastify/dist/ReactToastify.css';
import { useAtividadesFisicas } from '../../hooks/AtividadesContext';

registerLocale('pt', pt);

const options = [
  { value: 'corrida', label: 'Corrida' },
  { value: 'caminhada', label: 'Caminhada' },
  { value: 'musculação', label: 'Musculação' },
  { value: 'natacao', label: 'Natação' },
  { value: 'futebol', label: 'Futebol' },
  { value: 'basquete', label: 'Basquete' },
  { value: 'volei', label: 'Vôlei' },
  { value: 'futvolei', label: 'Futvôlei' },
  { value: 'luta', label: 'Luta' },
];

/**
 * Interface utilizada para tipar os valores do componente react-select
 */
interface Option {
  value: string;
  label: string;
}

/**
 * Componente que representa o painel principal do usuário. Mostra as
 * atividades do usuário, bem como permite a inserção de novas ou remoção.
 */
const Dashboard: React.FC = () => {
  /** Atributos de atividade */
  const [tempo, setTempo] = useState<number>(1);
  const [startDate, setStartDate] = useState(new Date());
  const [atividade, setAtividade] = useState<Option>({ value: '', label: '' });

  /** Variável para o contador do temp de atividades */
  const [totalTempoAtividades, setTotalTempoAtividades] = useState<number>(0);

  /** Contextos utilizados */
  const { usuario } = useAuth();
  const { atividadesContext, setAtividadesContext } = useAtividadesFisicas();

  /**
   * Hook que é disparado sempre que a tela é carregada novamente. Busca, na
   * API, todas as atividades para o usuário em sessão.
   */
  useEffect(() => {
    if (usuario) {
      api.get(`atividade?id=${usuario.id}`).then(response => {
        setAtividadesContext(
          response.data.sort((a, b) => {
            return new Date(b.data).getTime() - new Date(a.data).getTime();
          }),
        );
      });
    }
  }, []);

  /**
   * Toda vez que o contexto de atividades do usuário for modificado a soma
   * do total de tempo de exercícios é feito novemente.
   */
  useEffect(() => {
    const somaTempo = atividadesContext.reduce((acumulador, objetoAtual) => {
      return acumulador + objetoAtual.tempo_gasto;
    }, 0);
    setTotalTempoAtividades(somaTempo);
  }, [atividadesContext]);

  /**
   * Função executada quanto o evento onClick do botão 'adicionar' é acionado.
   * Caso a atividade esteja com todos os campos preenchidos, uma nova ativadade
   * é enviada a API para inserção no banco dedados para o usuário em sessão,
   * bem como a lista de atividades em tela é atualziada.
   */
  async function adicionarAtividade() {
    const atividadeDTO = new AtividadeDTO({
      usuario_id: usuario.id,
      data: startDate,
      tipo_atividade: atividade.label,
      tempo_gasto: tempo,
    });

    /**
     * Um atividade não pode ser inserida sem um tipo de atividade selecionada
     */
    if (!atividade.label) {
      toast.error('Adicione um tipo de atividade');
      return;
    }

    const atividadeSalvaComSucesso = await api.post('atividade', atividadeDTO);

    /** Verifica se houve retorno de sucesso da API */
    if (!atividadeSalvaComSucesso) {
      toast.warn('Erro ao inserir atividade. (400)');
      return;
    }
    const atividades = [
      ...atividadesContext,
      {
        id: atividadeSalvaComSucesso.data.id,
        data: atividadeSalvaComSucesso.data.data,
        tempo_gasto: atividadeSalvaComSucesso.data.tempo_gasto,
        tipo_atividade: atividadeSalvaComSucesso.data.tipo_atividade,
      },
    ];

    /**
     * Ordena as atividades por data
     */
    setAtividadesContext(
      atividades.sort((a, b) => {
        return new Date(b.data).getTime() - new Date(a.data).getTime();
      }),
    );

    toast.success('Atividade inserida');
  }

  /**
   * Função disparada sempre que o evento onClick do botão 'remover' é
   * disparado. Envia o identificador da atividade para API, para que seja
   * removida do banco de dados e remove a atividade da lista apresentada
   * em tela.
   * @param id identificador único da atividade
   */
  async function removerAtividade(id: string) {
    const resp = await api.delete(`atividade?id=${id}`);
    if (resp.data) {
      setAtividadesContext(atividadesContext.filter(item => item.id !== id));
      toast.success('Atividade removida');
    } else {
      toast.error('Erro ao remover');
    }
  }

  /**
   * Função disparada sempre que um novo tempo é digitado ou incrementado
   * no input de tempo
   * @param event
   */
  function handleInserirTempo(event: React.FormEvent<HTMLInputElement>) {
    const tempoInserido = parseInt(event.currentTarget.value, 10);
    setTempo(tempoInserido);
  }

  return (
    <div id="dashboard">
      <h3>{`Fala, ${usuario.nome}!  Vamos treinar?`}</h3>
      <fieldset id="box">
        <legend>Insira um item</legend>
        <div id="campos">
          <div className="inputTime">
            <p>Tempo gasto: </p>
            <input
              type="number"
              min={1}
              onChange={handleInserirTempo}
              value={tempo}
            />
            <p>min</p>
          </div>
          <div className="inputSelect">
            <p>Atividade: </p>
            <Select
              className="select"
              options={options}
              onChange={atv => setAtividade(atv as Option)}
            />
          </div>
          <div className="inputDate">
            <p>Dia: </p>
            <DatePicker
              dateFormat="dd/MM/yyyy"
              maxDate={new Date()}
              locale="pt"
              className="datePicker"
              selected={startDate}
              onChange={(date: Date) => setStartDate(date)}
            />
          </div>
        </div>
        <ToastContainer autoClose={3000} />
        <div className="button" onClick={adicionarAtividade}>
          Adicionar
        </div>
      </fieldset>
      {atividadesContext.length < 1 && <h3>Você não praticou atividades.</h3>}
      {atividadesContext.length > 0 && (
        <p>{`Parabéns! Você praticou ${totalTempoAtividades} minutos de atividade física até agora.`}</p>
      )}
      {atividadesContext.map((showAtividadeDTO: IShowAtividadeDTO) => (
        <div className="lista" key={showAtividadeDTO.id}>
          <div className="componente">
            <p>Dia:</p>
            <p>
              {`${new Date(showAtividadeDTO.data).getDate()}/${
                new Date(showAtividadeDTO.data).getMonth() + 1
              }`}
            </p>
          </div>
          <div className="componente">
            <p>atividade:</p>
            <p>{showAtividadeDTO.tipo_atividade}</p>
          </div>
          <div className="componente">
            <p>{showAtividadeDTO.tempo_gasto}</p>
            <p>minuto(s)</p>
          </div>
          <div
            className="componente"
            onClick={() => removerAtividade(showAtividadeDTO.id)}
          >
            <div>remover</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Dashboard;

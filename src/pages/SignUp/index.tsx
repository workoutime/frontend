/* eslint-disable react/style-prop-object */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import logo from '../../assets/running.svg';
import api from '../../services/api';
import 'react-toastify/dist/ReactToastify.css';

/**
 * Componente que representa a página de cadastro do usuário. De acordo
 * com os dados inseridos (nome, email e senha) os envia para API, para que
 * os dados sejam salvos no banco de dados.
 *
 * Utiliza a api: POST de usuários
 */
const SignUp: React.FC = () => {
  const [nome, setNome] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [senha, setSenha] = useState<string>('');
  const [isNome, setIsNome] = useState<boolean>(false);
  const [isEmail, setIsEmail] = useState<boolean>(false);
  const [isSenha, setIsSenha] = useState<boolean>(false);

  const history = useHistory();

  const handleSubmit = useCallback(async () => {
    if (isNome && isEmail && isSenha) {
      try {
        const response = await api.post('usuario', {
          nome,
          email,
          senha,
        });
        if (response) {
          toast.success('Cadastro realizado com sucesso');
        }
        history.push('/');
      } catch {
        toast.error('Email já cadastrado');
      }
    }
  }, [isNome, isEmail, isSenha]);

  function handleNomeChange(event: React.FormEvent<HTMLInputElement>) {
    const nomeInserido = event.currentTarget.value;
    setNome(nomeInserido);
    if (nomeInserido.length < 1) {
      setIsNome(false);
    } else {
      setIsNome(true);
    }
  }

  function handleEmailChange(event: React.FormEvent<HTMLInputElement>) {
    const emailInserido = event.currentTarget.value;
    setEmail(emailInserido);
    if (/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(email)) {
      setIsEmail(true);
    } else {
      setIsEmail(false);
    }
  }

  function handleSenhaChange(event: React.FormEvent<HTMLInputElement>) {
    const senhaInserida = event.currentTarget.value;
    setSenha(senhaInserida);
    if (senha.length < 5) {
      setIsSenha(false);
    } else {
      setIsSenha(true);
    }
  }

  return (
    <div id="container-signIn">
      <ToastContainer autoClose={3000} />
      <div id="signin-header">
        <img src={logo} alt="time" />
        <h1>Workoutime</h1>
      </div>
      <div id="form-container">
        <form id="form" autoComplete="off" method="post">
          <input
            placeholder="nome"
            type="text"
            value={nome}
            onChange={handleNomeChange}
          />
          {!isNome && <span>Insira um nome</span>}
          <input
            name="email"
            autoComplete="off"
            placeholder="e-mail"
            type="text"
            value={email || ''}
            onChange={handleEmailChange}
          />
          <input id="inputFake" name="email" autoComplete="off" />
          {!isEmail && <span>Informe um email válido</span>}
          <input
            name="senha"
            autoComplete="no"
            placeholder="senha"
            type="password"
            value={senha}
            onChange={handleSenhaChange}
          />
          <input
            id="inputFake"
            type="password"
            name="senha"
            autoComplete="no"
          />
          {!isSenha && <span>Informe um senha com 6 ou mais dígitos</span>}
          <div className="divButton" onClick={handleSubmit}>
            Cadastrar
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignUp;

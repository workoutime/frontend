/* eslint-disable import/no-duplicates */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState } from 'react';
import Dashboard from '../../components/Dashboard';
import Grafico from '../../components/Grafico';
import Header from '../../components/Header';

/**
 * Componente que representa a página principal. Dentro dessa página os
 * componentes serão apresentados de acordo com a escolha feita no menu do
 * header.
 *
 * Componente filho: Header.
 * É passado a função do tipo Dispatch do React (setState) para que, de
 * acordo com a opção que o usuário escolher no painel, o componente seja
 * apresentado na página.
 */
const Main: React.FC = () => {
  const [component, setComponent] = useState<string>('painel');
  return (
    <div id="container">
      <header>
        <Header setComponent={setComponent} />
      </header>
      <main>
        {component === 'painel' && <Dashboard />}
        {component === 'grafico' && <Grafico />}
      </main>
    </div>
  );
};

export default Main;

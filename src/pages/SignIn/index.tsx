/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import logo from '../../assets/running.svg';
import { useAuth } from '../../hooks/AuthContext';

/**
 * Componente que apresenta a página de login.
 * Utiliza o contexto de autenticação para que, de acordo com o email e senha
 * inseridos pelo usuário, ele possa se autenticar na API.
 * Há um Link de navegação para o componente de SignUp.
 */
const SignIn: React.FC = () => {
  const [email, setEmail] = useState<string>('');
  const [senha, setSenha] = useState<string>('');
  const [isUsuarioValido, setIsUsuarioValido] = useState<boolean>(true);

  const { signIn } = useAuth();

  const history = useHistory();

  const handleSubmit = async () => {
    if (email && senha) {
      try {
        await signIn({ email, senha });
        history.push('main');
        toast.success('Login realizado com sucesso');
      } catch (err) {
        setIsUsuarioValido(false);
        toast.error('Usuario ou senha inválidos');
      }
    }
  };

  function handleEmailChange(event: React.FormEvent<HTMLInputElement>) {
    const emailInserido = event.currentTarget.value;
    setEmail(emailInserido);
  }

  function handleSenhaChange(event: React.FormEvent<HTMLInputElement>) {
    const senhaInserida = event.currentTarget.value;
    setSenha(senhaInserida);
  }

  return (
    <div id="container-signIn">
      <div id="signin-header">
        <img src={logo} alt="" />
        <h1>Workoutime</h1>
      </div>
      <div id="form-container">
        <div id="form">
          <input
            placeholder="e-mail"
            type="text"
            value={email}
            onChange={handleEmailChange}
          />

          <input
            type="password"
            placeholder="senha"
            value={senha}
            onChange={handleSenhaChange}
          />

          {!isUsuarioValido && <span>Usuário ou senha inválidos</span>}

          <div className="divButton" title="entrar" onClick={handleSubmit}>
            Entrar
          </div>

          <ToastContainer autoClose={2000} />

          <Link to="/signUp" id="signUp">
            <p>Cadastre-se</p>
          </Link>
        </div>
      </div>
      <p>Controle das suas atividades físicas diárias.</p>
    </div>
  );
};

export default SignIn;

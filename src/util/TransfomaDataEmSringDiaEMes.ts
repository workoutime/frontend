/**
 * @returns a tada formatada em uma string com dia e mes e ano
 * @param data uma data no formato string
 */
const transformaData = (data: string): string => {
  return `${new Date(data).getDate().toString()}/${(
    new Date(data).getMonth() + 1
  ).toString()}/${new Date(data).getFullYear().toString().substr(2)}`;
};

export default transformaData;

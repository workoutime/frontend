/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import IAtividadesPorData from '../models/IAtividadesPorData';
import transformaData from './TransfomaDataEmSringDiaEMes';

/**
 * @returns Um array com as atividades separadas por data com a soma do tempo
 * de atividades pela data.
 * @param atividades um array do tipo IShowAtividadeDTO.
 */
const montarArrayDeAtividadesPorData = atividades => {
  const atividadesPorData: IAtividadesPorData[] = [];
  atividades.forEach(atividade => {
    const atividadeComDataJaExistente = atividadesPorData.filter(
      atv => atv.data === transformaData(atividade.data),
    );
    if (atividadeComDataJaExistente[0]) {
      atividadeComDataJaExistente[0].tempo += atividade.tempo_gasto;
    } else {
      atividadesPorData.push({
        data: transformaData(atividade.data),
        tempo: atividade.tempo_gasto,
      });
    }
  });
  console.log('ARRRAY ORDENADO: ', atividadesPorData);
  return atividadesPorData;
};

export default montarArrayDeAtividadesPorData;

/* eslint-disable react/prop-types */
import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useState,
} from 'react';
import IShowAtividadeDTO from '../DTO/IShowAtividadeDTO';

/** Interface que defini o tipo para o contexto de atividades */
interface IAtividadeContext {
  atividadesContext: IShowAtividadeDTO[];
  setAtividadesContext: Dispatch<SetStateAction<IShowAtividadeDTO[]>>;
}

/** Será importado e utilizado para os componentes que forem se utilizar
 * desse contexto.
 */
const AtividadesContext = createContext<IAtividadeContext>(
  {} as IAtividadeContext,
);

/**
 * Componente que será inserido dentro do componente principal para que o
 * contexto possa ser utilizado por outros componentes.
 * @param children
 */
const AtividadesProvider: React.FC = ({ children }) => {
  const [atividadesContext, setAtividadesContext] = useState<
    IShowAtividadeDTO[]
  >([]);

  return (
    <AtividadesContext.Provider
      value={{
        atividadesContext,
        setAtividadesContext,
      }}
    >
      {children}
    </AtividadesContext.Provider>
  );
};

/**
 * Hook criado para facilitar a chamada do contexto de atividades físicas
 * do usuário
 */
function useAtividadesFisicas(): IAtividadeContext {
  const context = useContext(AtividadesContext);

  /**
   * Se o desenvolvedor utilizar o AuthContext sem adicionar o component
   * AuthProvider por fora do componente que o estiver utilizando, um erro
   * será gerado.
   */
  if (!context) {
    throw new Error(
      'O hook useAuth deve ser usado dentro do componente AuthProvider',
    );
  }

  /**
   * Se ele conseguiu encontrar, retorna-se o contexto
   */
  return context;
}

export { useAtividadesFisicas, AtividadesProvider };

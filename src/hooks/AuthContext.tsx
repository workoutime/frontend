/* eslint-disable react/prop-types */
import React, { createContext, useCallback, useContext, useState } from 'react';
import IAuthStateDTO from '../DTO/IAuthStateDTO';
import ISessionDTO from '../DTO/ISessionDTO';
import ISignInDTO from '../DTO/ISignInDTO';
import api from '../services/api';

/**
 * Inteface que define a tipagem para o contexto de autenticação do usuário.
 */
interface IAuthContext {
  usuario: ISessionDTO;
  signIn(credenciais: ISignInDTO): Promise<void>;
  signOut(): void;
}

/**
 * Será importada por outros componentes que necessitem desse contexto.
 */
export const AuthContext = createContext<IAuthContext>({} as IAuthContext);

/**
 * Componente que será inserido no componente principal, para que outros
 * componentes possam se utilizar desse contexto.
 * @param param0
 */
const AuthProvider: React.FC = ({ children }) => {
  /**
   * Caso os dados do usuário já estejam setados no localStorage do browser,
   * o componente já será inicializado com os dados do usuário. Dessa forma
   * permiteirá que o usuário já será direcionado para o painel principal.
   */
  const [data, setData] = useState<IAuthStateDTO>(() => {
    const token = localStorage.getItem('@Workoutime:token');
    const usuarioEncontrado = localStorage.getItem('@Workoutime:usuario');

    if (token && usuarioEncontrado) {
      return { token, usuarioEncontrado: JSON.parse(usuarioEncontrado) };
    }

    return {} as IAuthStateDTO;
  });

  /**
   * Função que irá enviar o email e senha inserido pelo usuário para a API
   * de autenticação. Caso os dados estejam válidos, insere-os no localStorage
   * do browser e permite que o usuário mantenha a navegação.
   */
  const signIn = useCallback(async ({ email, senha }) => {
    const response = await api.post('session', { email, senha });

    const { token, usuarioEncontrado } = response.data;

    localStorage.setItem('@Workoutime:token', token);
    localStorage.setItem(
      '@Workoutime:usuario',
      JSON.stringify(usuarioEncontrado),
    );

    setData({ token, usuarioEncontrado });
  }, []);

  /**
   * Função de logout. Remove os dados do usuário do localStorage.
   */
  const signOut = useCallback(() => {
    localStorage.removeItem('@Workoutime:token');
    localStorage.removeItem('@Workoutime:usuario');

    setData({} as IAuthStateDTO);
  }, []);
  return (
    <AuthContext.Provider
      value={{
        usuario: data.usuarioEncontrado as ISessionDTO,
        signIn,
        signOut,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

/**
 * Hook criado para facilitar a chamada do contexto de autenticação
 */
function useAuth(): IAuthContext {
  const context = useContext(AuthContext);

  /**
   * Se o desenvolvedor utilizar o AuthContext sem adicionar o component
   * AuthProvider por fora do componente que o estiver utilizando, um erro
   * será gerado.
   */
  if (!context) {
    throw new Error(
      'O hook useAuth deve ser usado dentro do componente AuthProvider',
    );
  }

  /**
   * Se ele conseguiu encontrar, retorna-se o contexto
   */
  return context;
}

export { useAuth, AuthProvider };

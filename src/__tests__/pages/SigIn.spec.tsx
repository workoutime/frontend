import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import SignIn from '../../pages/SignIn';

const mockedHistoryPush = jest.fn();
const mockedSignIn = jest.fn();
/**
 * Mock para forjar um useHistory e um Link do react-router-dom
 */
jest.mock('react-router-dom', () => {
  return {
    /**
     * jest.fn() retorna uma função vazia só para saber se o useHistory
     * foi chamado ou não
     */
    useHistory: () => ({
      push: mockedHistoryPush,
    }),
    /**
     * Cria um componente fictício do Link
     * ReactNode: É um tipo que generaliza qualquer conteúdo que um
     * componente poderia receber.
     */
    Link: ({ children }: { children: React.ReactNode }) => children,
  };
});

jest.mock('../../hooks/AuthContext', () => {
  return {
    useAuth: () => ({
      signIn: mockedSignIn,
    }),
  };
});

/**
 * O que esse teste faz?
 * Simula a inserção de um email e senha pelo usuário e um clique no botão
 * de entrar. Dessa forma a página será redirecionada pelo Link do
 * react-router-dom para a página main. Essa é a verificação feita dentro
 * do expect
 */
describe('Testes para a página de login', () => {
  /**
   * Ao final de cada teste limpa a nossa constante que simula o useHistory
   */
  beforeEach(() => {
    mockedHistoryPush.mockClear();
  });

  it('Ao se autenticar o usuário deve ser redirecionado para a página main', async () => {
    const { getAllByPlaceholderText, getByTitle } = render(<SignIn />);

    const campoEmail = getAllByPlaceholderText('e-mail')[0];
    const campoSenha = getAllByPlaceholderText('senha')[0];
    const botaoEntrar = getByTitle('entrar');

    fireEvent.change(campoEmail, { target: { value: 'test@email.com' } });
    fireEvent.change(campoSenha, { target: { value: '123456' } });

    fireEvent.click(botaoEntrar);

    await waitFor(() => {
      expect(mockedHistoryPush).toHaveBeenCalledWith('main');
    });
  });

  it('O usuário não deve se autenticar com um email inválido', async () => {
    const { getAllByPlaceholderText, getByTitle } = render(<SignIn />);

    const campoEmail = getAllByPlaceholderText('e-mail')[0];
    const campoSenha = getAllByPlaceholderText('senha')[0];
    const botaoEntrar = getByTitle('entrar');

    fireEvent.change(campoEmail, { target: { value: 'email-invalido' } });
    fireEvent.change(campoSenha, { target: { value: '123456' } });

    fireEvent.click(botaoEntrar);

    await waitFor(() => {
      expect(mockedHistoryPush).not.toHaveBeenCalledWith('main');
    });
  });
});

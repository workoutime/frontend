import { renderHook, act } from '@testing-library/react-hooks';
import MockAdapter from 'axios-mock-adapter';
import { AuthProvider, useAuth } from '../../hooks/AuthContext';
import api from '../../services/api';

const apiMock = new MockAdapter(api);

describe('Testes para o hook de autenticação', () => {
  it('O usuário deve conseguir fazer o login', async () => {
    const apiResponse = {
      usuarioEncontrado: {
        id: '8824b6fd-b098-4794-b9e3-2e1b175c9372',
        nome: 'Teste',
        email: 'teste@gmail.com',
        created_at: '2020-10-30T14:29:45.547Z',
        updated_at: '2020-10-30T14:29:45.547Z',
      },
      token: 'tokent3st3',
    };
    apiMock.onPost('session').reply(200, apiResponse);

    /**
     * Mock para a função setItem do localStorage
     */
    const setItemSpy = jest.spyOn(Storage.prototype, 'setItem');

    /**
     * Faz o mock do nosso context de autenticação
     */
    const { result, waitForNextUpdate } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    /**
     * Mock da execução da funçaõ de signIn
     */
    result.current.signIn({
      email: 'teste@gmail.com',
      senha: '123456',
    });

    /**
     * Aguarda até qua alguma modificação seja feita, para que o expect
     * seja disparado
     */
    await waitForNextUpdate();

    expect(setItemSpy).toHaveBeenCalledWith(
      '@Workoutime:token',
      apiResponse.token,
    );
    expect(setItemSpy).toHaveBeenCalledWith(
      '@Workoutime:usuario',
      JSON.stringify(apiResponse.usuarioEncontrado),
    );
    expect(result.current.usuario.email).toEqual('teste@gmail.com');
  });

  it('Deve recuperar os dados de localStorage se o usuário já estiver logado', () => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(key => {
      switch (key) {
        case '@Workoutime:token':
          return 'tokent3st3';
        case '@Workoutime:usuario':
          return JSON.stringify({
            id: '8824b6fd-b098-4794-b9e3-2e1b175c9372',
            nome: 'Teste',
            email: 'teste@gmail.com',
          });
        default:
          return null;
      }
    });

    /**
     * Faz o mock do nosso context de autenticação
     */
    const { result } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    expect(result.current.usuario.email).toEqual('teste@gmail.com');
  });

  it('Deve ser possível fazer o logout da apicação', async () => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(key => {
      switch (key) {
        case '@Workoutime:token':
          return 'tokent3st3';
        case '@Workoutime:usuario':
          return JSON.stringify({
            id: '8824b6fd-b098-4794-b9e3-2e1b175c9372',
            nome: 'Teste',
            email: 'teste@gmail.com',
          });
        default:
          return null;
      }
    });

    const removeItemSpy = jest.spyOn(Storage.prototype, 'removeItem');

    /**
     * Faz o mock do nosso context de autenticação
     */
    const { result, waitForNextUpdate } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    act(() => {
      result.current.signOut();
    });

    expect(removeItemSpy).toHaveBeenCalledTimes(2);
    expect(result.current.usuario).toBeUndefined();
  });
});

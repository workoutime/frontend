export default interface IAtividade {
  usuario_id: string;
  data: Date;
  tipo_atividade: string;
  tempo_gasto: number;
  id: string;
  created_at: Date;
  updated_at: Date;
}

export default interface IAuthStateDTO {
  token: string;
  usuarioEncontrado: object;
}

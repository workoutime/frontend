export default interface IUsuarioDTO {
  nome: string;
  email: string;
  senha: string;
}

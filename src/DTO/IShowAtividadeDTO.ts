export default interface IShowAtividadeDTO {
  id: string;
  data: Date;
  tipo_atividade: string;
  tempo_gasto: number;
  totalTempo?: number;
}

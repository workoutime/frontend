/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export default class AtividadeDTO {
  usuario_id: string;

  data: Date;

  tipo_atividade: string;

  tempo_gasto: number;

  constructor({ usuario_id, data, tipo_atividade, tempo_gasto }) {
    this.usuario_id = usuario_id;
    this.data = data;
    this.tipo_atividade = tipo_atividade;
    this.tempo_gasto = tempo_gasto;
  }
}

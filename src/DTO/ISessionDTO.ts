export default interface ISessionDTO {
  id: string;
  nome: string;
  email: string;
  created_at: string;
  updated_at: string;
}

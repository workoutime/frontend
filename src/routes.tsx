import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Main from './pages/Main';
import { AuthProvider } from './hooks/AuthContext';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';

import Route from './Route';
import { AtividadesProvider } from './hooks/AtividadesContext';

const Routes: React.FC = () => {
  return (
    <BrowserRouter>
      <AuthProvider>
        <Route component={SignIn} path="/" exact />
        <Route component={SignUp} path="/signUp" />
        <AtividadesProvider>
          <Route component={Main} isPrivate path="/main" />
        </AtividadesProvider>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default Routes;
